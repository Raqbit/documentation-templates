# Realisatie Document (__Projectnaam__)

```text
Leereenheid: NAAM LEEREENHEID
Versienummer: 1.0
Auteur(s): Ramon Kluijtmans
Datom: DATUM
```

```text
COMPETENTIES

1. Competentie ‘Analyseren’
    a. Vertaalt de informatie uit het functioneel en technisch ontwerp naar oplossingen
voor de behoeften/wensen van de opdrachtgever
2. Competentie ‘Plannen en organiseren’
    b. Plant de werkzaamheden en activiteiten voor het realiseren van de applicatie
    c. Maakt een inschatting van de benodigde tijd om de applicatie(s) te realiseren binnen de
       beschikbare tijd
3. Competentie ‘Materialen en middelen inzetten’
    d. Kiest en maakt gebruik van materialen en middelen opdat de gerealiseerde applicatie aan de
       gestelde eisen voldoet
4. Competentie ‘Op de behoeften en verwachtingen van de “klant” richten’
    e. Stemt de voortgang in één tot twee gesprekken af met de opdrachtgever en bewaakt
       daarmee de behoeften, wensen en eisen van de opdrachtgever
5. Competentie ‘Met druk en tegenslag omgaan’
    f. Blijft gericht op de werkzaamheden en is productief onder (tijds)druk en/of in een stressvolle
       omgeving
6. Competentie ‘Kwaliteit leveren’
    g. Realiseert de applicatie die voldoet aan de eisen die in de opdracht en in het
       functioneel en technisch ontwerp vermeld staan
7. Competentie ‘Formuleren en rapporteren’
    h) Documenteert de werkzaamheden en resultaten voor, tijdens en na de realisatie
       van de applicatie
```

## Versiebeheer

| Datum | Versie | Wijziging | Wie |
| - | - | - | - |
| DATUM | 1.0 | Initial version | Ramon |

## Inleiding

> *Beschrijf kort waar het project over gaat*
>
> *Beschrijf waarvoor dit document dient*

## Realisatielog

> *Beschrijf gedetailleerd per gerealiseerde functie, hoe lang de bouw van de
applicatie heeft geduurd*

## Verslag realisatie proces

> *Beschrijf hoe je zelf het realisatie proces hebt ervaren. Waar liep je tegen
aan? Wat ging er goed? Was je initiële planning realistisch of niet?*

## Wijzigingen ten op zichten van FO en TO

> *Beschrijf hier alle verschillen tussen FO, TO en de gerealiseerde applicatie.*

## Beschrijving diverse software functies

> *Maak met behulp van screenshots een gebruikers handleiding. Beschrijf de
werking van de volledige applicatie.*

## Software oplossingen

> *Beschrijf de koppeling met database/databestand of andere bestanden,
(netwerk)communicatie protocollen, complexe software code als multi-threading, etc.*