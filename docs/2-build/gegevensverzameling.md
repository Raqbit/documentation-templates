# Gegevensverzameling (__Projectnaam__)

```text
Leereenheid: NAAM LEEREENHEID
Versienummer: 1.0
Auteur(s): AUTEUR
Datom: DATUM
```

```text
COMPETENTIES

1. Competentie ‘Analyseren’
    a. Toetst de gegevens en bepaalt of deze verwerkt kunnen worden in een
       gegevensverzameling/database
    b. Bedenkt haalbare oplossingen om een gegevensverzameling/database te realiseren
2. Competentie ‘Onderzoeken’
    c. Onderzoekt het gebruik van verschillende applicaties en de functionele werkwijze
       daarvan
3. Competentie ‘Op de behoeften en verwachtingen van de “klant” richten’
    d. Vertaalt de behoeften/wensen van de opdrachtgever naar een werkende
       gegevensverzameling/database
4. Competentie ‘Samenwerken en overleggen’
    e. Informeert in één gesprek de leidinggevende/projectleider over de werkwijze van de
       gegevensverzameling/database en de procedures voor het aanleveren van gegevens
5. Competentie ‘Formuleren en rapporteren’
    f. Documenteert de aangelegde gegevensverzameling/database
```

## Versiebeheer

| Datum | Versie | Wijziging | Wie |
| - | - | - | - |
| DATUM | 1.0 | Initial version | Bram |

## Inleiding

In dit document wordt vastgelegd welke data zal worden overgenomen uit bestaande
systemen, welke nieuwe data aangemaakt zal worden voor de te maken applicatie en
welke test data zal worden gebruikt. Dit alles met als doel om de applicatie te
kunnen testen en daarna in gebruik te kunnen nemen.

> *Beschrijf kort waar het project over gaat, of kopieer dat uit eerdere
documenten binnen dit project.*

## Analyse

### Onderzoek

> *Hier moet je vastleggen welke data je hebt geanalyseerd en welke van die data
bruikbaar is en welke niet. Je mag hierbij verwijzen naar externe (data)bestanden.*

### Realisatie

> *Hier geef je aan  waar je de geschikt bevonden data in de opslag (bv database)
van de te maken applicate gaat opslaan. Als je  bijvoorbeeld in het Technisch
Ontwerp een ERD (Entity Relation Diagram) voor een database hebt opgesteld, kun
je hier aangeven welke data in welke tabel wordt opgeslagen. Ook geef je aan
waar nieuwe data zal worden opgeslagen. Je mag hierbij verwijzen naar externe
(data)bestanden. Verwijs ook naar het ERD in het Technisch Ontwerp en controleer
of deze nog volledig is.*

## Applicaties

> *Beschrijf hier welke applicaties en welke werkwijzen je gaat gebruiken om de
data in de gegevensopslag (bv database) te krijgen.*

## Resultaat

> *Leg hier vast waar de voltooide gegevensopslag (bv database) te vinden is. In
deze gegevensopslag is alle data (of een essentiële, met de opdrachtgever
overlegde, hoeveelheid) opgeslagen. Daarmee kan aangetoond worden dat alle data
in de gegevensopslag opgeslagen kan worden en kan later de applicatie getest
worden. Maak hierbij bijvoorbeeld gebruik van een screenshot van de database met
de data en/of verwijs naar een script met alle data.*

## Aanlevering

> *Bepaal hier hoe nieuwe data aangeleverd moet worden en door wie, zodat deze
aan de opgezette  gegevensopslag toegevoegd kan worden. Denk hierbij aan
bestandsformaten en volgorde van kolommen. Beschrijf ook wie (of welke rollen)
de aangeleverde data in de database kan zetten. Je mag hierbij verwijzen naar
externe (data)bestanden.*
