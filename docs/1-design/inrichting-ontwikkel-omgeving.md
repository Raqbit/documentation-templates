# Inrichting Ontwikkel Omgeving (__Projectnaam__)

```text
Leereenheid: NAAM LEEREENHEID
Versienummer: 1.0
Auteur(s): Ramon Kluijtmans
Datom: DATUM
```

```text
COMPETENTIES

1. Competentie ‘Plannen en organiseren’
    a. Bewaakt de activiteiten en voortgang van de werkzaamheden binnen de
       ontwikkelomgeving
    b. Houdt voor het inrichten rekening met de haalbaarheid en mogelijke veranderingen
2. Competentie ‘Materialen en middelen inzetten’
    c. Kiest en maakt gebruik van materialen en middelen om een
       ontwikkelomgeving in te richten dat aansluit op het technisch ontwerp
3. Competentie ‘Samenwerken en overleggen’
    d. Stemt de werkzaamheden af met de leidinggevende/projectleider en informeert deze
       over de voortgang in twee tot drie gesprekken
4. Competentie ‘Kwaliteit leveren’
    e. Laat de resultaten aansluiten op de behoeften/wensen van de
       opdrachtgever volgens de eisen die in het functioneel en technisch
       ontwerp
    f. Signaleert en rapporteert afwijkingen aan leidinggevende/projectleider
5. Competentie ‘Formuleren en rapporteren’
    g. Documenteert de ingerichte ontwikkelomgeving
```

## Versiebeheer

| Datum | Versie | Wijziging | Wie |
| - | - | - | - |
| DATUM | 1.0 | Initial version | Ramon |

## Inleiding

Dit document beschrijft hoe de ontwikkelomgeving moet zijn ingericht zodat de
software kan worden gerealiseerd.

Het gaat hier om alle aspecten die nodig zijn voor de ontwikkelomgeving.

Dit document is beschreven voor het geval dat de bestaande ontwikkelomgeving
niet meer zou functioneren zodat er een nieuwe omgeving kan worden opgezet.

## Hardware componenten ontwikkelomgeving

> *Welke hardware is er nodig zodat alle functionaliteiten kunnen ontwikkeld
worden en getest op de ontwikkelomgeving.*
>
> *Lijst met per hardware component alle specifieke kenmerken.*

## Softwarecomponenten ontwikkelomgeving

> *Welke software wordt gebruikt voor het ontwikkelen van het product.*
>
> *Lijst met versienummers.*

## Schema ontwikkelomgeving