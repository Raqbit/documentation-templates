# Technisch Ontwerp (__Projectnaam__) `ENGELS`

```text
Leereenheid: NAAM LEEREENHEID
Versienummer: 1.0
Auteur(s): Ramon Kluijtmans
Datom: DATUM
```

```text
COMPETENTIES

1. Competentie ‘Plannen en organiseren’
    a. Stelt doelen en een chronologische planning van de benodigde tijd op van de
activiteiten voor het opleveren van het ontwerp
    b. Stelt mensen en middelen vast die nodig zijn voor het opleveren van het ontwerp
2. Competentie ‘Analyseren’
    c. Zet de verkregen informatie (uit opdracht 1 en opdracht 3) om in
       oplossingen voor de behoeften/wensen van de opdrachtgever. Verwerkt
       deze oplossingen in het ontwerp
3. Competentie ‘Formuleren en rapporteren’
    d. Documenteert het technisch ontwerp
4. Competentie ‘Overtuigen en beïnvloeden’
    e. Komt met ideeën aansluitend op het functioneel ontwerp
    f. Brengt deze ideeën onderbouwend en beargumenterend over
5. Competentie ‘Presenteren’
    g. Presenteert het technisch ontwerp aan de leidinggevende/projectleider
```

## Versiebeheer

| Datum | Versie | Wijziging | Wie |
| - | - | - | - |
| DATUM | 1.0 | Initial version | Ramon |

## Inleiding

Het doel van het technisch ontwerp is om inzicht te krijgen in de verschillende
technieken die gebruikt gaan worden bij ontwikkelen van de gevraagde software.
Daarnaast zal er een duidelijk beeld worden geschetst hoe de verschillende
technieken met elkaar verbonden zijn. Dit document is voor deze opdracht gemaakt
zodat een ieder die betrokken gaat worden bij dit project makkelijk zijn weg zal
vinden in de al bestaande architectuur en software functies.

> *Beschrijving in het kort het project.*

## Planning

> *Geef en tijdsindicatie aan over de duur van het maken van dit document.
Beschrijf per onderdeel van het document hoeveel tijd je gaat gebruiken voor de
realisatie ervan.*

## Software Architectuur

![Architecture](images/architecture.jpg)

## Application Flow

> *Maak of een applicatie flow of...*

![Application Flow](images/application-flow.png)

## Activity Diagrams

> *of maak activity diagrams.*

![Activity Diagrams](images/activity-diagram.png)

## UI Design

> *User Interface moet niet voor ieder scherm worden gemaakt. Enkel voor het
meest uitgebreide scherm (dus niet enkel het inlogscherm).*

![Login Screen](images/login-screen.png)

## Database / Gegevens / Content

Beschrijf de tabellen en hun onderlinge relaties, met kolommen en per kolom
minimaal: datatype + veldlengte, verplicht of niet, primary keys, foreign keys,
en eventueel relevante overige informatie.

Geef een overzicht d.m.v een databasediagram (ERD).

![ERD](images/erd.png)

Als je afwijkt van de 3e normaalvorm, licht dan toe waarom dit gebeurt. ([Databasenormalisatie](https://nl.wikipedia.org/wiki/Databasenormalisatie))

## Klassen Diagram

![Class Diagram](images/class-diagram.png)

## Gegevens

> *Welke externe data bestanden worden gebruikt om de applicatie te laten
functioneren en/of de database te vullen. Beschrijf waar deze data vandaan komt
en wie deze aanlevert.*

## Applicatie Opbouw

![Application Architecture](images/application-architecture.png)

## Akkoord leidinggevende / Projectleider

Naam

---

Datum

---

Handtekening

---